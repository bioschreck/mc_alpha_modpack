# Start with leather armor.
mods.initialinventory.InvHandler.addStartingItem(<minecraft:leather_helmet>, 39);
mods.initialinventory.InvHandler.addStartingItem(<minecraft:leather_chestplate>, 38);
mods.initialinventory.InvHandler.addStartingItem(<minecraft:leather_leggings>, 37);
mods.initialinventory.InvHandler.addStartingItem(<minecraft:leather_boots>, 36);

mods.initialinventory.InvHandler.addStartingItem(<minecraft:wooden_sword>, 0);
mods.initialinventory.InvHandler.addStartingItem(<minecraft:bow>, 1);
mods.initialinventory.InvHandler.addStartingItem(<minecraft:arrow> * 16, 28);
mods.initialinventory.InvHandler.addStartingItem(<minecraft:wooden_pickaxe>, 2);
mods.initialinventory.InvHandler.addStartingItem(<minecraft:wooden_axe>, 3);
mods.initialinventory.InvHandler.addStartingItem(<minecraft:wooden_shovel>, 4);
mods.initialinventory.InvHandler.addStartingItem(<minecraft:wooden_hoe>,32);
mods.initialinventory.InvHandler.addStartingItem(<minecraft:fishing_rod>, 5);


mods.initialinventory.InvHandler.addStartingItem(<minecraft:bucket>, 6);
mods.initialinventory.InvHandler.addStartingItem(<minecraft:bread> * 3, 7);
mods.initialinventory.InvHandler.addStartingItem(<minecraft:torch> * 5, 8);


mods.initialinventory.InvHandler.addStartingItem(<minecraft:log> * 4, 27);


mods.initialinventory.InvHandler.addStartingItem(<natura:seed_bags>* 3, 31);


mods.initialinventory.InvHandler.addStartingItem(<wearablebackpacks:backpack>, 9);
mods.initialinventory.InvHandler.addStartingItem(<sereneseasons:season_clock>, 10);
mods.initialinventory.InvHandler.addStartingItem(<flatcoloredblocks:coloredcraftingitem>, 16);
mods.initialinventory.InvHandler.addStartingItem(<chisel:chisel_iron>, 17);
