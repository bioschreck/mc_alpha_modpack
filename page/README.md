# Alpha Modpack

![Modpack](http://bioschreck.com/mc/alpha_modpack.png)

<br>
<br>

[Alpha Modpack Launcher](http://bioschreck.com/mc/launcher-4.5-SNAPSHOT.jar)  <br>
Sources on [Gitlab](https://gitlab.com/bioschreck/mc_alpha_modpack) 